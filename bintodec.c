/* Binary to decimal conversion program */
#include<stdio.h>

int main()
{
    long int bin;
    int i, j digit, dec=0;
    printf("Enter binary number: ");
    scanf("%ld", &bin);
    for(i=0; bin>0; i++, bin/=10)
    {
        digit = bin%10;
        for(j=i; j>0; --j)  digit *= 2;
        dec += digit;
    }
    printf("Decimal: %d", dec);
    return 0;
}
