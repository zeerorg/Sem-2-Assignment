/* decimal to binary conversion */
#include <stdio.h>
int main()
{
    long int bin = 0;
    int i, j, digit, dec=0;
    printf("Enter Decimal: ");
    scanf("%d", &dec);
    bin = 0;
    for(i=0; dec>0; ++i, dec /= 2){

        digit = dec%2;
        for(j=i; j>0; --j)    digit *= 10;
        bin += digit;
    }
    printf("Binary: %ld", bin);
    return 0;
}
