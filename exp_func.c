//Program for exponential function

#include<stdio.h>

int main()
{
    int num, pow;
    printf("Number: ");
    scanf("%d", &num);
    printf("Power: ");
    scanf("%d", &pow);
    int ans = 1;

    for(int i = 1; i <= pow; ++i)
        ans *= num;

    printf("%d to the power %d is: %d",num, pow, ans);

    return 0;
}
