// Program to compute GCD of two numbers

#include<stdio.h>
int main()
{
    int num1, num2;
    printf("Enter two numbers: ");
    scanf("%d %d", &num1, &num2);
    int ans = 1;
    for(int i = 2; i <= num1 && i <= num2; ++i )
        if(num1 % i == 0 && num2 % i == 0)
            ans = i;

    printf("GCD of %d and %d is %d", num1, num2, ans);

    return 0;
}
