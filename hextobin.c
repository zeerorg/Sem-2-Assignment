/* Hexadecimal to binary */
#include <stdio.h>
int main()
{
    int i, j, digit, dec;
    long int bin;
    printf("Enter Hexadecimal number: ");
    scanf("%X", &dec);
    printf("Decimal: %d", dec);
    bin = 0;
    for(i=0; dec>0; ++i, dec /= 2){

        digit = dec%2;
        for(j=i; j>0; --j)    digit *= 10;
        bin += digit;
    }
    printf("\nBinary: %ld", bin);
    return 0;
}
