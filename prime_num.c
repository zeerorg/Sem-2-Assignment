//Program to find if number is prime

#include<stdio.h>

int main()
{
    int num, flag = 0;
    printf("Enter number: ");
    scanf("%d", &num);
    for(int i = 2; i <= num/2 && flag == 0; ++i)
        if(num % i == 0)
            flag = 1;

    if (flag == 0)
        printf("Prime");
    else
        printf("Composite");
    return 0;
}
