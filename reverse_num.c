// Program To reverse a number

#include<stdio.h>

int main()
{
    int num;
    printf("Enter number to reverse: ");
    scanf("%d", &num);
    int rev = 0, i = num;
    while(i > 0)
    {
        rev *= 10;
        rev += i % 10;
        i /= 10;
    }

    printf("Reverse of number %d is %d", num, rev);
    return 0;
}
